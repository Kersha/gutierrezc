//
//  ViewController.swift
//  gutierrezC
//
//  Created by carlos on 5/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var Titulo: UILabel!
    @IBOutlet weak var fecha: UILabel!
    var modeloDato = modeloDatos()
    var nextLink=""
    
    override func viewDidLoad() {
         //modeloDato.obtenerInfoUno()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //setValues()
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let apiInfo = try? JSONDecoder().decode(infoUno.self, from: data) else {
                print ("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.Titulo.text = apiInfo.viewTitle
                self.fecha.text = apiInfo.date
                self.nextLink = apiInfo.nextLink
                //print(self.Titulo,self.fecha,self.enlace)
            }
        }
        task.resume()
    }

    @IBAction func clicBotonNext(_ sender: Any) {
        performSegue(withIdentifier: "pantallaDos", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setValues(){
        fecha.text = modeloDato.fecha
        Titulo.text = modeloDato.Titulo
        nextLink = modeloDato.enlace
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "pantallaDos"{
            let destinationController = segue.destination as! ViewControllerTabla
            destinationController.link = nextLink
            destinationController.nombre = "\(Name.text!)"
        }
    }
}


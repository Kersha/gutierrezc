//
//  modeloDatos.swift
//  gutierrezC
//
//  Created by carlos on 6/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import Foundation

class modeloDatos  {
    var Titulo = ""
    var fecha = ""
    var enlace = ""
    
    func obtenerInfoUno(){
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let apiInfo = try? JSONDecoder().decode(infoUno.self, from: data) else {
                print ("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                self.Titulo = apiInfo.viewTitle
                self.fecha = apiInfo.date
                self.enlace = apiInfo.nextLink
                print(self.Titulo,self.fecha,self.enlace)
            }
        }
        task.resume()
        
    }
    
    var resultadoSuma=0
    var arregloLabels:[String]=[]
    var arregloValues:[Int]=[]
    var elementos = 0
    
    func obtenerInfoDos(link:String){
        let urlString = link
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let apiInfo = try? JSONDecoder().decode(infoDos.self, from: data) else {
                print ("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                for link in apiInfo.data{
                    self.arregloLabels.append(link.label)
                    self.resultadoSuma = self.resultadoSuma+link.value
                    self.arregloValues.append(link.value)
                    self.elementos += 1
                }
                self.resultadoSuma = self.resultadoSuma/self.elementos
                
            }
            
            
        }
        task.resume()
        
    }
    
}

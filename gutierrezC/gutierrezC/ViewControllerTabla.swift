//
//  ViewControllerTabla.swift
//  gutierrezC
//
//  Created by carlos on 6/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewControllerTabla: UIViewController , UITableViewDataSource, UITableViewDelegate{

    

    var arregloLabels : [String] = []
    var arregloValues : [Int] = []
    var elementos = 0
    var link : String?
    var nombre : String?
    var resultadoSuma = 0
    
    @IBOutlet weak var promedio: UILabel!
    @IBOutlet weak var tituloHola: UILabel!
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let apiInfo = try? JSONDecoder().decode(infoDos.self, from: data) else {
                print ("Error decoding")
                return
            }
            
            DispatchQueue.main.async {
                for link in apiInfo.data{
                    self.arregloLabels.append(link.label)
                    self.resultadoSuma = self.resultadoSuma+link.value
                    self.arregloValues.append(link.value)
                    self.elementos += 1
                }
                self.resultadoSuma = self.resultadoSuma/self.elementos
                self.promedio.text = "Promedio: " + String.init(self.resultadoSuma)
                self.tituloHola.text = "Hi, " + self.nombre!
                self.tableview.reloadData();
            }
        }
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elementos
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        
        cell.label.text = arregloLabels[indexPath.row]
        cell.value.text = String.init(arregloValues[indexPath.row])
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
